import java.util.Random;

class FortuneWheel {
    private final int WHEEL_IDLE = 0;
    private final int WHEEL_ROLLING = 1;
    private final int WHEEL_STOPPING_SLOWLY = 2;
    private final int WHEEL_STOP = 3;
    private int wheelStatus;
    private int guessedNumber;

    public FortuneWheel(){
        this.wheelStatus = WHEEL_IDLE;
    }
    public void rollTheWheel(int guessedNumber){
        if(guessedNumber > 9){
            this.guessedNumber = 9;
        } else if(guessedNumber < 0){
            this.guessedNumber = 0;
        } else{
            this.guessedNumber = guessedNumber;
        }

        if(this.wheelStatus == WHEEL_IDLE){
            System.out.println("Wheel start spinning.....!");
            this.wheelStatus = WHEEL_ROLLING;
        } else if (this.wheelStatus == WHEEL_STOP){
            System.out.println("Wheel start spinning again.....!");
            this.wheelStatus = WHEEL_ROLLING;
        }
        else{
            System.out.println("Invalid command. Do nothing here!");
        }
    }
    public void instantStop(){
        if(this.wheelStatus == WHEEL_ROLLING) {
            System.out.println("Wheel stopped!");
            this.wheelStatus = WHEEL_STOP;
        }else{
            System.out.println("Invalid command. Do nothing here!");
        }
    }
    public void stopSlowly(){
        if(this.wheelStatus == WHEEL_ROLLING){
            System.out.println("3...2...1...Wheel stopped!");
            this.wheelStatus = WHEEL_STOPPING_SLOWLY;
        }else{
            System.out.println("Invalid command. Do nothing here!");
        }
    }
    public void rollingResult(){
        int wheelResult = 0;
        Random random = new Random();
        if(this.wheelStatus == WHEEL_STOP){
            wheelResult = random.nextInt(9);
        }else if (this.wheelStatus == WHEEL_STOPPING_SLOWLY){
            for (int i = 0; i < 3; i++){
                wheelResult = random.nextInt(9);
            }
        }else{
            System.out.println("Invalid command. Do nothing here!");
        }
        this.wheelStatus = WHEEL_IDLE;
        System.out.println("Wheel stopped at number..."+Integer.toString(wheelResult));
        if(wheelResult == this.guessedNumber){
            System.out.println("Congratulations, you hit jackpot!");
        }else{
            System.out.println("Sorry, better luck if you spin again!");
        }
    }
}
public class FortuneWheelSimulation {
    public static void main(String[] args){
        FortuneWheel wheelAdam = new FortuneWheel();
        wheelAdam.instantStop();
        wheelAdam.rollTheWheel(6);
        wheelAdam.instantStop();
        wheelAdam.rollingResult();
    }
}

public abstract class State{
    public FortuneWheel fortuneWheel;
    public State(fortuneWheel){
        this.fortuneWheel=fortuneWheel;
    }
    abstract void rollTheWheel(int num);
    abstract void instantStop();
    abstract void stopSlowly();
    abstract void rollingResult();
  }
 public class WHEEL_IDLE extends State{
      public void rollTheWheel(int number){
        System.out.println("Wheel start spinning.....!");
        this.fortuneWheel.setState(new WheelStop(this.fortuneWheel));
      }
      public void instantStop(){
          
    }
    public void stopSlowly(){
          
    }
    public void rollingResult(){
          
    }
 }
}
